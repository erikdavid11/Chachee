# Be sure to restart your server when you modify this file.

# Add new inflection rules using the following format. Inflections
# are locale specific, and you may define rules for as many different
# locales as you wish. All of these examples are active by default:
ActiveSupport::Inflector.inflections(:en) do |inflect|
    inflect.irregular 'persona', 'personas'
      inflect.irregular 'telefono', 'telefonos'
      inflect.irregular 'colonia', 'colonias'
      inflect.irregular 'archivo', 'archivos'
      inflect.irregular 'ciudad', 'ciudades'
      inflect.irregular 'legal', 'legales'
      inflect.irregular 'direccion', 'direcciones'
      inflect.irregular 'bancario', 'bancarios'
      inflect.irregular 'delegacion', 'delegaciones'
      inflect.irregular 'producto', 'productos'
      inflect.irregular 'categoria', 'categorias'
      inflect.irregular 'tipo', 'tipos'
      inflect.irregular 'marca', 'marcas'
      inflect.irregular 'cliente', 'clientes'
      inflect.irregular 'socio', 'socios'
      inflect.irregular 'coloniacobertura', 'coloniacoberturas'
#   inflect.plural /^(ox)$/i, '\1en'
#   inflect.singular /^(ox)en/i, '\1'
#   inflect.irregular 'person', 'people'
  inflect.uncountable %w( dashboard )
end

# These inflection rules are supported but not enabled by default:
# ActiveSupport::Inflector.inflections(:en) do |inflect|
#   inflect.acronym 'RESTful'
# end
