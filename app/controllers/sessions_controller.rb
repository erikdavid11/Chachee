class SessionsController < Devise::SessionsController
  # layout :choose_layout

  def new
    self.resource = resource_class.new(sign_in_params)
    clean_up_passwords(resource)
    respond_with(resource, serialize_options(resource))
  end



  def create

    # raise StandardError, "params[:user][:email]"

      self.resource = warden.authenticate!(auth_options)
      set_flash_message(:notice, :signed_in) if is_navigational_format?
      sign_in(resource_name, resource)
      # respond_with resource, :location => after_sign_in_path_for(resource)
      if current_user.roles.map(&:name) == ["Cliente"]
        redirect_to cliente_index_path
      elsif current_user.roles.map(&:name) == ["Socia"]
        redirect_to  socios_index_path
      elsif current_user.roles.map(&:name) == ["root"]
        redirect_to  dashboard_index_path
      else
        respond_with resource, :location => after_sign_in_path_for(resource)
      end

  end

  # private
  # def choose_layout
  #   if params[:candidato] == "si"
  #     'candidato'
  #   else
  #     'webp'
  #   end
  # end



end