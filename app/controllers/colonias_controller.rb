class ColoniasController < ApplicationController
  before_action :set_colonia, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  before_action :verificar_usuario

  # GET /colonias
  # GET /colonias.json
  def index
    @colonias = Colonia.all
  end

  # GET /colonias/1
  # GET /colonias/1.json
  def show
  end

  # GET /colonias/new
  def new
    @colonia = Colonia.new
  end

  # GET /colonias/1/edit
  def edit
  end

  # POST /colonias
  # POST /colonias.json
  def create
    @colonia = Colonia.new(colonia_params)

    respond_to do |format|
      if @colonia.save
        format.html { redirect_to @colonia, notice: 'Colonia was successfully created.' }
        format.json { render :show, status: :created, location: @colonia }
      else
        format.html { render :new }
        format.json { render json: @colonia.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /colonias/1
  # PATCH/PUT /colonias/1.json
  def update
    respond_to do |format|
      if @colonia.update(colonia_params)
        format.html { redirect_to @colonia, notice: 'Colonia was successfully updated.' }
        format.json { render :show, status: :ok, location: @colonia }
      else
        format.html { render :edit }
        format.json { render json: @colonia.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /colonias/1
  # DELETE /colonias/1.json
  def destroy
    @colonia.destroy
    respond_to do |format|
      format.html { redirect_to colonias_url, notice: 'Colonia was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_colonia
      @colonia = Colonia.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def colonia_params
      params.require(:colonia).permit(:nombre, :ciudad_id)
    end

    def verificar_usuario
      if user_signed_in? && current_user.roles.map(&:name) == ["Socia"]

      else
        redirect_to root_path, :alert => "No autorizado"
      end
    end

end
