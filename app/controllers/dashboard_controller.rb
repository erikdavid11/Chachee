class DashboardController < ApplicationController
  before_action :authenticate_user!
  before_action :verificar_usuario


  def index
  end

  private
  def verificar_usuario
    if user_signed_in? && current_user.roles.map(&:name) == ["root"]

    else
      redirect_to root_path, :alert => "No autorizado"
    end
  end


end
