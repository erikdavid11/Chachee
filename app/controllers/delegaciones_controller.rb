class DelegacionesController < ApplicationController
  before_action :set_delegacion, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  before_action :verificar_usuario

  # GET /delegaciones
  # GET /delegaciones.json
  def index
    @delegaciones = Delegacion.all
  end

  # GET /delegaciones/1
  # GET /delegaciones/1.json
  def show
  end

  # GET /delegaciones/new
  def new
    @delegacion = Delegacion.new
  end

  # GET /delegaciones/1/edit
  def edit
  end

  # POST /delegaciones
  # POST /delegaciones.json
  def create
    @delegacion = Delegacion.new(delegacion_params)

    respond_to do |format|
      if @delegacion.save
        format.html { redirect_to @delegacion, notice: 'Delegacion was successfully created.' }
        format.json { render :show, status: :created, location: @delegacion }
      else
        format.html { render :new }
        format.json { render json: @delegacion.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /delegaciones/1
  # PATCH/PUT /delegaciones/1.json
  def update
    respond_to do |format|
      if @delegacion.update(delegacion_params)
        format.html { redirect_to @delegacion, notice: 'Delegacion was successfully updated.' }
        format.json { render :show, status: :ok, location: @delegacion }
      else
        format.html { render :edit }
        format.json { render json: @delegacion.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /delegaciones/1
  # DELETE /delegaciones/1.json
  def destroy
    @delegacion.destroy
    respond_to do |format|
      format.html { redirect_to delegaciones_url, notice: 'Delegacion was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_delegacion
      @delegacion = Delegacion.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def delegacion_params
      params.require(:delegacion).permit(:ciudad_id, :nombre)
    end

    def verificar_usuario
      if user_signed_in? && current_user.roles.map(&:name) == ["root"]

      else
        redirect_to root_path, :alert => "No autorizado"
      end
    end

end
