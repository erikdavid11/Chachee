class ClienteController < ApplicationController
  before_action :authenticate_user!
  before_action :verificar_usuario
  layout 'Por favor ingresa el nombre del layou'


  def index
  end

  private
  def verificar_usuario
    if user_signed_in? && current_user.roles.map(&:name) == ["Cliente"]

    else
      redirect_to root_path, :alert => "No autorizado"
    end
  end


end
