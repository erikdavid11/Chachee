json.array!(@delegaciones) do |delegacion|
  json.extract! delegacion, :id, :ciudad_id, :nombre
  json.url delegacion_url(delegacion, format: :json)
end
