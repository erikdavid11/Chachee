json.array!(@direcciones) do |direccion|
  json.extract! direccion, :id, :colonia, :calle, :cp, :delegacion_id
  json.url direccion_url(direccion, format: :json)
end
