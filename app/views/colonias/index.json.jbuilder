json.array!(@colonias) do |colonia|
  json.extract! colonia, :id, :nombre, :ciudad_id
  json.url colonia_url(colonia, format: :json)
end
