class Ciudad < ActiveRecord::Base
  has_many :delegaciones
  has_many :colonias
end
