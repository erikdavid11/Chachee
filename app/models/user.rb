class User < ActiveRecord::Base
  rolify
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  attr_accessor :manejo

  has_many :telefonos, :as=> :telefeable, dependent: :destroy
  has_many :coloniacoberturas, dependent: :destroy
  has_many :legal, dependent: :destroy
  has_many :direcciones, :as=> :direcceable, dependent: :destroy
  has_many :archivos, :as=> :archieable, dependent: :destroy

  accepts_nested_attributes_for :direcciones,  :reject_if => lambda { |attrs| attrs.all? { |key, value| value.blank? }}
  accepts_nested_attributes_for :archivos,  :reject_if => lambda { |attrs| attrs.all? { |key, value| value.blank? }}
  accepts_nested_attributes_for :telefonos,    :reject_if => lambda { |attrs| attrs.all? { |key, value| value.blank? }}
  accepts_nested_attributes_for :coloniacoberturas,    :reject_if => lambda { |attrs| attrs.all? { |key, value| value.blank? }}
  accepts_nested_attributes_for :legal,    :reject_if => lambda { |attrs| attrs.all? { |key, value| value.blank? }}

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  after_create :set_default_role


    def self.cambiar_rol(usuario, antiguorol, nuevorol)
      usuario = User.find_by(id: usuario)
      usuario.revoke antiguorol.to_sym
      usuario.add_role nuevorol.to_sym
    end


  private
   def set_default_role
     user = User.find_by(id: id)
     if manejo == 1
       user.add_role "Cliente"
     else
       user.add_role "Socia"
     end


   end
end
