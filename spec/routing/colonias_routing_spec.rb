require "rails_helper"

RSpec.describe ColoniasController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/colonias").to route_to("colonias#index")
    end

    it "routes to #new" do
      expect(:get => "/colonias/new").to route_to("colonias#new")
    end

    it "routes to #show" do
      expect(:get => "/colonias/1").to route_to("colonias#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/colonias/1/edit").to route_to("colonias#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/colonias").to route_to("colonias#create")
    end

    it "routes to #update" do
      expect(:put => "/colonias/1").to route_to("colonias#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/colonias/1").to route_to("colonias#destroy", :id => "1")
    end

  end
end
