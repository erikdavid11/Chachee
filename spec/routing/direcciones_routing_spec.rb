require "rails_helper"

RSpec.describe DireccionesController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/direcciones").to route_to("direcciones#index")
    end

    it "routes to #new" do
      expect(:get => "/direcciones/new").to route_to("direcciones#new")
    end

    it "routes to #show" do
      expect(:get => "/direcciones/1").to route_to("direcciones#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/direcciones/1/edit").to route_to("direcciones#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/direcciones").to route_to("direcciones#create")
    end

    it "routes to #update" do
      expect(:put => "/direcciones/1").to route_to("direcciones#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/direcciones/1").to route_to("direcciones#destroy", :id => "1")
    end

  end
end
