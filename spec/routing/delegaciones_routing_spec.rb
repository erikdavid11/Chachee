require "rails_helper"

RSpec.describe DelegacionesController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/delegaciones").to route_to("delegaciones#index")
    end

    it "routes to #new" do
      expect(:get => "/delegaciones/new").to route_to("delegaciones#new")
    end

    it "routes to #show" do
      expect(:get => "/delegaciones/1").to route_to("delegaciones#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/delegaciones/1/edit").to route_to("delegaciones#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/delegaciones").to route_to("delegaciones#create")
    end

    it "routes to #update" do
      expect(:put => "/delegaciones/1").to route_to("delegaciones#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/delegaciones/1").to route_to("delegaciones#destroy", :id => "1")
    end

  end
end
