require 'rails_helper'

RSpec.describe "delegaciones/new", type: :view do
  before(:each) do
    assign(:delegacion, Delegacion.new(
      :ciudad => nil,
      :nombre => "MyString"
    ))
  end

  it "renders new delegacion form" do
    render

    assert_select "form[action=?][method=?]", delegaciones_path, "post" do

      assert_select "input#delegacion_ciudad_id[name=?]", "delegacion[ciudad_id]"

      assert_select "input#delegacion_nombre[name=?]", "delegacion[nombre]"
    end
  end
end
