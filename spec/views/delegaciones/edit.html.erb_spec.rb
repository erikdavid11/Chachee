require 'rails_helper'

RSpec.describe "delegaciones/edit", type: :view do
  before(:each) do
    @delegacion = assign(:delegacion, Delegacion.create!(
      :ciudad => nil,
      :nombre => "MyString"
    ))
  end

  it "renders the edit delegacion form" do
    render

    assert_select "form[action=?][method=?]", delegacion_path(@delegacion), "post" do

      assert_select "input#delegacion_ciudad_id[name=?]", "delegacion[ciudad_id]"

      assert_select "input#delegacion_nombre[name=?]", "delegacion[nombre]"
    end
  end
end
