require 'rails_helper'

RSpec.describe "delegaciones/index", type: :view do
  before(:each) do
    assign(:delegaciones, [
      Delegacion.create!(
        :ciudad => nil,
        :nombre => "Nombre"
      ),
      Delegacion.create!(
        :ciudad => nil,
        :nombre => "Nombre"
      )
    ])
  end

  it "renders a list of delegaciones" do
    render
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => "Nombre".to_s, :count => 2
  end
end
