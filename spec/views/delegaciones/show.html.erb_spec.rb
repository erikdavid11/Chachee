require 'rails_helper'

RSpec.describe "delegaciones/show", type: :view do
  before(:each) do
    @delegacion = assign(:delegacion, Delegacion.create!(
      :ciudad => nil,
      :nombre => "Nombre"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(//)
    expect(rendered).to match(/Nombre/)
  end
end
