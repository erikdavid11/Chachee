require 'rails_helper'

RSpec.describe "colonias/edit", type: :view do
  before(:each) do
    @colonia = assign(:colonia, Colonia.create!(
      :nombre => "MyString",
      :ciudad => nil
    ))
  end

  it "renders the edit colonia form" do
    render

    assert_select "form[action=?][method=?]", colonia_path(@colonia), "post" do

      assert_select "input#colonia_nombre[name=?]", "colonia[nombre]"

      assert_select "input#colonia_ciudad_id[name=?]", "colonia[ciudad_id]"
    end
  end
end
