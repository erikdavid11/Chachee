require 'rails_helper'

RSpec.describe "colonias/index", type: :view do
  before(:each) do
    assign(:colonias, [
      Colonia.create!(
        :nombre => "Nombre",
        :ciudad => nil
      ),
      Colonia.create!(
        :nombre => "Nombre",
        :ciudad => nil
      )
    ])
  end

  it "renders a list of colonias" do
    render
    assert_select "tr>td", :text => "Nombre".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
