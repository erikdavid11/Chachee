require 'rails_helper'

RSpec.describe "colonias/show", type: :view do
  before(:each) do
    @colonia = assign(:colonia, Colonia.create!(
      :nombre => "Nombre",
      :ciudad => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Nombre/)
    expect(rendered).to match(//)
  end
end
