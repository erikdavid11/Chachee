require 'rails_helper'

RSpec.describe "colonias/new", type: :view do
  before(:each) do
    assign(:colonia, Colonia.new(
      :nombre => "MyString",
      :ciudad => nil
    ))
  end

  it "renders new colonia form" do
    render

    assert_select "form[action=?][method=?]", colonias_path, "post" do

      assert_select "input#colonia_nombre[name=?]", "colonia[nombre]"

      assert_select "input#colonia_ciudad_id[name=?]", "colonia[ciudad_id]"
    end
  end
end
