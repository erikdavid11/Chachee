require 'rails_helper'

RSpec.describe "direcciones/show", type: :view do
  before(:each) do
    @direccion = assign(:direccion, Direccion.create!(
      :colonia => "Colonia",
      :calle => "Calle",
      :cp => "Cp",
      :delegacion => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Colonia/)
    expect(rendered).to match(/Calle/)
    expect(rendered).to match(/Cp/)
    expect(rendered).to match(//)
  end
end
