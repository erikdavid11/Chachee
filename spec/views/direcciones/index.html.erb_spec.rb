require 'rails_helper'

RSpec.describe "direcciones/index", type: :view do
  before(:each) do
    assign(:direcciones, [
      Direccion.create!(
        :colonia => "Colonia",
        :calle => "Calle",
        :cp => "Cp",
        :delegacion => nil
      ),
      Direccion.create!(
        :colonia => "Colonia",
        :calle => "Calle",
        :cp => "Cp",
        :delegacion => nil
      )
    ])
  end

  it "renders a list of direcciones" do
    render
    assert_select "tr>td", :text => "Colonia".to_s, :count => 2
    assert_select "tr>td", :text => "Calle".to_s, :count => 2
    assert_select "tr>td", :text => "Cp".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
