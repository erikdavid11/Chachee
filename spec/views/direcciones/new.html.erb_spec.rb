require 'rails_helper'

RSpec.describe "direcciones/new", type: :view do
  before(:each) do
    assign(:direccion, Direccion.new(
      :colonia => "MyString",
      :calle => "MyString",
      :cp => "MyString",
      :delegacion => nil
    ))
  end

  it "renders new direccion form" do
    render

    assert_select "form[action=?][method=?]", direcciones_path, "post" do

      assert_select "input#direccion_colonia[name=?]", "direccion[colonia]"

      assert_select "input#direccion_calle[name=?]", "direccion[calle]"

      assert_select "input#direccion_cp[name=?]", "direccion[cp]"

      assert_select "input#direccion_delegacion_id[name=?]", "direccion[delegacion_id]"
    end
  end
end
