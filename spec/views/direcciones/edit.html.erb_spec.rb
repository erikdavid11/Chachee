require 'rails_helper'

RSpec.describe "direcciones/edit", type: :view do
  before(:each) do
    @direccion = assign(:direccion, Direccion.create!(
      :colonia => "MyString",
      :calle => "MyString",
      :cp => "MyString",
      :delegacion => nil
    ))
  end

  it "renders the edit direccion form" do
    render

    assert_select "form[action=?][method=?]", direccion_path(@direccion), "post" do

      assert_select "input#direccion_colonia[name=?]", "direccion[colonia]"

      assert_select "input#direccion_calle[name=?]", "direccion[calle]"

      assert_select "input#direccion_cp[name=?]", "direccion[cp]"

      assert_select "input#direccion_delegacion_id[name=?]", "direccion[delegacion_id]"
    end
  end
end
