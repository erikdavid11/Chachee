require 'rails_helper'

RSpec.describe "ciudades/edit", type: :view do
  before(:each) do
    @ciudad = assign(:ciudad, Ciudad.create!(
      :nombre => "MyString"
    ))
  end

  it "renders the edit ciudad form" do
    render

    assert_select "form[action=?][method=?]", ciudad_path(@ciudad), "post" do

      assert_select "input#ciudad_nombre[name=?]", "ciudad[nombre]"
    end
  end
end
