require 'rails_helper'

RSpec.describe "ciudades/index", type: :view do
  before(:each) do
    assign(:ciudades, [
      Ciudad.create!(
        :nombre => "Nombre"
      ),
      Ciudad.create!(
        :nombre => "Nombre"
      )
    ])
  end

  it "renders a list of ciudades" do
    render
    assert_select "tr>td", :text => "Nombre".to_s, :count => 2
  end
end
