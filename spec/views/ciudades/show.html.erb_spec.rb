require 'rails_helper'

RSpec.describe "ciudades/show", type: :view do
  before(:each) do
    @ciudad = assign(:ciudad, Ciudad.create!(
      :nombre => "Nombre"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Nombre/)
  end
end
