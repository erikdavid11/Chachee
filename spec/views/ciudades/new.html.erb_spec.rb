require 'rails_helper'

RSpec.describe "ciudades/new", type: :view do
  before(:each) do
    assign(:ciudad, Ciudad.new(
      :nombre => "MyString"
    ))
  end

  it "renders new ciudad form" do
    render

    assert_select "form[action=?][method=?]", ciudades_path, "post" do

      assert_select "input#ciudad_nombre[name=?]", "ciudad[nombre]"
    end
  end
end
