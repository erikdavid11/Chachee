class CreateTelefonos < ActiveRecord::Migration
  def change
    create_table :telefonos do |t|
      t.string :tipo
      t.string :numero
      t.references :telefeable, polymorphic: true, index: true

      t.timestamps null: false
    end
  end
end
