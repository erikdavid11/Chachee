class CreateColonias < ActiveRecord::Migration
  def change
    create_table :colonias do |t|
      t.string :nombre
      t.references :ciudad, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
