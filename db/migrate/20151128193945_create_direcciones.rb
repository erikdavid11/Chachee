class CreateDirecciones < ActiveRecord::Migration
  def change
    create_table :direcciones do |t|
      t.string :colonia
      t.string :calle
      t.string :cp
      t.references :delegacion, index: true, foreign_key: true
      t.references :direcceable, polymorphic: true, index: true

      t.timestamps null: false
    end
  end
end
