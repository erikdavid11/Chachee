class CreateLegales < ActiveRecord::Migration
  def change
    create_table :legales do |t|
      t.string :curp
      t.string :rfc
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
