class CreateArchivos < ActiveRecord::Migration
  def change
    create_table :archivos do |t|
      t.string :archivo
      t.string :tipo
      t.references :archieable, polymorphic: true, index: true

      t.timestamps null: false
    end
  end
end
