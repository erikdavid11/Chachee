class CreateDelegaciones < ActiveRecord::Migration
  def change
    create_table :delegaciones do |t|
      t.references :ciudad, index: true, foreign_key: true
      t.string :nombre

      t.timestamps null: false
    end
  end
end
