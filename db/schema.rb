# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151129033952) do

  create_table "archivos", force: :cascade do |t|
    t.string   "archivo"
    t.string   "tipo"
    t.integer  "archieable_id"
    t.string   "archieable_type"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "archivos", ["archieable_type", "archieable_id"], name: "index_archivos_on_archieable_type_and_archieable_id"

  create_table "ciudades", force: :cascade do |t|
    t.string   "nombre"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "coloniacoberturas", force: :cascade do |t|
    t.integer  "colonia_id"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "coloniacoberturas", ["colonia_id"], name: "index_coloniacoberturas_on_colonia_id"
  add_index "coloniacoberturas", ["user_id"], name: "index_coloniacoberturas_on_user_id"

  create_table "colonias", force: :cascade do |t|
    t.string   "nombre"
    t.integer  "ciudad_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "colonias", ["ciudad_id"], name: "index_colonias_on_ciudad_id"

  create_table "delegaciones", force: :cascade do |t|
    t.integer  "ciudad_id"
    t.string   "nombre"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "delegaciones", ["ciudad_id"], name: "index_delegaciones_on_ciudad_id"

  create_table "direcciones", force: :cascade do |t|
    t.string   "colonia"
    t.string   "calle"
    t.string   "cp"
    t.integer  "delegacion_id"
    t.integer  "direcceable_id"
    t.string   "direcceable_type"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  add_index "direcciones", ["delegacion_id"], name: "index_direcciones_on_delegacion_id"
  add_index "direcciones", ["direcceable_type", "direcceable_id"], name: "index_direcciones_on_direcceable_type_and_direcceable_id"

  create_table "legales", force: :cascade do |t|
    t.string   "curp"
    t.string   "rfc"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "legales", ["user_id"], name: "index_legales_on_user_id"

  create_table "roles", force: :cascade do |t|
    t.string   "name"
    t.integer  "resource_id"
    t.string   "resource_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "roles", ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id"
  add_index "roles", ["name"], name: "index_roles_on_name"

  create_table "telefonos", force: :cascade do |t|
    t.string   "tipo"
    t.string   "numero"
    t.integer  "telefeable_id"
    t.string   "telefeable_type"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "telefonos", ["telefeable_type", "telefeable_id"], name: "index_telefonos_on_telefeable_type_and_telefeable_id"

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "nombres"
    t.string   "apellidopaterno"
    t.string   "apellidomaterno"
    t.date     "fechanacimiento"
    t.string   "tipo"
    t.string   "sexo"
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true

  create_table "users_roles", id: false, force: :cascade do |t|
    t.integer "user_id"
    t.integer "role_id"
  end

  add_index "users_roles", ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id"

end
