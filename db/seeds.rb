# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
puts     "Generando Ciudades"
estado = Ciudad.create([ {id: "1", nombre: "Tijuana"},
                         {id: "2", nombre: "Mexicali"},
                         {id: "3", nombre: "Ensenada"},
                         {id: "4", nombre: "Rosarito"},
                         {id: "5", nombre: "Distrito Federal"},
                         {id: "6", nombre: "Aguas Calientes"},
                         {id: "7", nombre: "Ecatepec"},
                         {id: "8", nombre: "Guadalajara"},
                         {id: "9", nombre: "Puebla"},
                         {id: "10", nombre: "Juárez"},
                         {id: "11", nombre: "León"},
                         {id: "12", nombre: "Zapopan"},
                         {id: "13", nombre: "Monterrey"},
                         {id: "14", nombre: "Nezahualcóyotl"},
                         {id: "15", nombre: "Chihuahua"},
                         {id: "16", nombre: "Naucalpan"},
                         {id: "17", nombre: "Mérida"},
                         {id: "18", nombre: "San Luis Potosí"},
                         {id: "19", nombre: "Aguascalientes"},
                         {id: "20", nombre: "Hermosillo"},
                         {id: "21", nombre: "Saltillo"},
                         {id: "22", nombre: "Culiacán"},
                         {id: "23", nombre: "Guadalupe"},
                         {id: "24", nombre: "Acapulco"},
                         {id: "25", nombre: "Tlalnepantla"},
                         {id: "26", nombre: "Cancún"},
                         {id: "27", nombre: "Querétaro"},
                         {id: "28", nombre: "Chimalhuacán"},
                         {id: "29", nombre: "Torreón"},
                         {id: "30", nombre: "Morelia"},
                         {id: "31", nombre: "Reynosa"},
                         {id: "32", nombre: "Tlaquepaque"},
                         {id: "33", nombre: "Tuxtla"},
                         {id: "34", nombre: "Durango"},
                         {id: "35", nombre: "Toluca"},
                         {id: "36", nombre: "Ciudad López Mateos"},
                         {id: "37", nombre: "Cuautitlán Izcalli"},
                         {id: "38", nombre: "Apodaca"},
                         {id: "39", nombre: "Matamoros"},
                         {id: "40", nombre: "San Nicolás de los Garza"},
                         {id: "41", nombre: "Veracruz"},
                         {id: "42", nombre: "Xalapa"},
                         {id: "43", nombre: "Tonalá"},
                         {id: "44", nombre: "Mazatlán"},
                         {id: "45", nombre: "Irapuato"},
                         {id: "46", nombre: "Nuevo Laredo"},
                         {id: "47", nombre: "Xico"},
                         {id: "48", nombre: "Villahermosa"},
                         {id: "49", nombre: "General Escobedo"},
                         {id: "50", nombre: "Celaya"},
                         {id: "51", nombre: "Cuernavaca"},
                         {id: "52", nombre: "Tepic"},
                         {id: "53", nombre: "Ixtapaluca"},
                         {id: "54", nombre: "Ciudad Victoria"},
                         {id: "55", nombre: "Ciudad Obregón"},
                         {id: "56", nombre: "Tampico"},
                         {id: "57", nombre: "Villa Nicolás Romero"},
                         {id: "58", nombre: "San Francisco Coacalco"},
                         {id: "59", nombre: "Santa Catarina"},
                         {id: "60", nombre: "Uruapan"},
                         {id: "61", nombre: "Gómez Palacio"},
                         {id: "62", nombre: "Los Mochis"},
                         {id: "63", nombre: "Pachuca"},
                         {id: "64", nombre: "Oaxaca"},
                         {id: "65", nombre: "Soledad de Graciano Sánchez"},
                         {id: "66", nombre: "Tehuacán"},
                         {id: "67", nombre: "Ojo de Agua"},
                         {id: "68", nombre: "Campeche"},
                         {id: "69", nombre: "Coatzacoalcos"},
                         {id: "70", nombre: "Monclova"},
                         {id: "71", nombre: "La Paz"},
                         {id: "72", nombre: "Nogales"},
                         {id: "73", nombre: "Buenavista"},
                         {id: "74", nombre: "Puerto Vallarta"},
                         {id: "75", nombre: "Tapachula"},
                         {id: "76", nombre: "Ciudad Madero"},
                         {id: "77", nombre: "San Pablo de las Salinas"},
                         {id: "78", nombre: "Chilpancingo"},
                         {id: "79", nombre: "Poza Rica de Hidalgo"},
                         {id: "80", nombre: "Chicoloapan"},
                         {id: "81", nombre: "Ciudad del Carmen"},
                         {id: "82", nombre: "Chalco"},
                         {id: "83", nombre: "Jiutepec"},
                         {id: "84", nombre: "Salamanca"},
                         {id: "85", nombre: "San Luis Río Colorado"},
                         {id: "86", nombre: "San Cristóbal de las Casas"},
                         {id: "87", nombre: "Cuautla"},
                         {id: "88", nombre: "Juárez"},
                         {id: "89", nombre: "Chetumal"},
                         {id: "90", nombre: "Piedras Negras"},
                         {id: "91", nombre: "Playa del Carmen"},
                         {id: "92", nombre: "Zamora"},
                         {id: "93" , nombre: "Córdoba"},
                         {id: "94", nombre: "San Juan del Río"},
                         {id: "95", nombre: "Colima"},
                         {id: "96", nombre: "Ciudad Acuña"},
                         {id: "97", nombre: "Manzanillo"},
                         {id: "98", nombre: "Zacatecas"},
                         {id: "99", nombre: "Veracruz"},
                         {id: "100", nombre: "Ciudad Valles"},
                         {id: "101", nombre: "Guadalupe"},
                         {id: "102", nombre: "San Pedro Garza García"},
                         {id: "103", nombre: "Naucalpan de Juárez"},
                         {id: "104", nombre: "Fresnillo"},
                         {id: "105", nombre: "Orizaba"},
                         {id: "106", nombre: "Miramar"},
                         {id: "107", nombre: "Iguala"},
                         {id: "108", nombre: "Delicias"},
                         {id: "109", nombre: "Villa de Álvarez"},
                         {id: "110", nombre: "Cuauhtémoc"},
                         {id: "111", nombre: "Navojoa"},
                         {id: "112", nombre: "Guaymas"},
                         {id: "113", nombre: "Minatitlán"},
                         {id: "114", nombre: "Cuautitlán"},
                         {id: "115", nombre: "Texcoco de Mora"},
                         {id: "116", nombre: "Hidalgo del Parral"},
                         {id: "117", nombre: "Tepexpan"},
                         {id: "118", nombre: "Tulancingo de Bravo"},
                         {id: "119", nombre: "San Juan Bautista Tuxtepec"}

                       ])

# puts "Creando delegaciones"
# delegaciones = Delegacion.create([
#                                      {id: '1', nombre: 'Zona Centro', ciudad_id: '1'},
#                                      {id: '2', nombre: 'Col Independencia', ciudad_id: '1'},
#                                      {id: '3', nombre: 'Otra Delegacion', ciudad_id: '2'},
#                                      {id: '4', nombre: 'Otra Delegacion', ciudad_id: '2'},
#
#                                  ])



puts "Creando Roles"
roles = Role.create([ {id: "1", name: "root" },
                      {id: "2", name: "Cliente" },
                      {id: "3", name: "Socia" }
                    ])

puts "Creando usuarios default"
usuarios = User.create([

                           {id:'1', email: 'obelich@gmail.com', password: 'oscarm2128', password_confirmation: 'oscarm2128'},
                           {id:'2', email: 'root@mail.com', password: 'Unpassword1234', password_confirmation: 'Unpassword1234'},
                           {id:'3', email: 'cliente@mail.mx', password: 'cliente1234', password_confirmation: 'cliente1234'},
                       ])

puts "Asignando Roles Administrativos"
usuario = User.find_by(email: 'obelich@gmail.com')
usuario.remove_role :Invitado
usuario.add_role :root


usuario1 = User.find_by(email: 'root@mail.com')
usuario1.remove_role :Invitado
usuario1.add_role :root



# puts 'Limpiando primary keys'
# ActiveRecord::Base.connection.tables.each do |t|
#   ActiveRecord::Base.connection.reset_pk_sequence!(t)
# end;nil
